import keras
keras.__version__

import cv2
import numpy as np
import sklearn
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import csv
import random
import matplotlib.pyplot as plt

import keras.backend as K
from keras.backend import tf as ktf
from keras.models import Sequential
from keras.layers import Lambda, Cropping2D, Conv2D, MaxPooling2D, Flatten, Dense, ELU, BatchNormalization, Dropout
from keras.optimizers import Adam
from keras.models import load_model

def get_train_validation_samples(driving_log_path):
       
    samples = []
    with open(driving_log_path) as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)  # skip the header
        for line in reader:
            samples.append(line)

    train_samples, validation_samples = train_test_split(samples, test_size=0.2)
    
    return train_samples, validation_samples

def randomize_color_channel(X_img):    
    X_img_copy = X_img.copy()  
    row, col, nchan = X_img_copy.shape    
    return X_img_copy[:,:,np.random.permutation(nchan)]

def alter_luminance(X_img, w1, w2):
    X_img_copy = X_img.copy() 
    X_img_hsv = cv2.cvtColor(X_img_copy, cv2.COLOR_RGB2HSV)
    X_img_copy = X_img_copy.astype(np.float32)
    X_img_hsv = X_img_hsv.astype(np.float32)
    X_img_copy /= 255.0
    X_img_hsv /= 255.0
    row, col, ch = X_img.shape
    new_img = cv2.addWeighted(X_img_copy, w1, X_img_hsv, w2, 0)
    new_img = new_img*255.0
    new_img = new_img.astype(np.uint8)
    
    return new_img 

def get_augmented_data_nvidia(image_bgr, m):
    image_aug = []
    m_aug = []
    ran_scale = 0.05
    image_yuv = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2YUV); m_yuv = m + random.uniform(0, ran_scale)
    image_yuv_r = randomize_color_channel(image_yuv); m_yuv_r = m + random.uniform(0, ran_scale)
    image_yuv_l1 = alter_luminance(image_yuv, w1=0.9, w2=0.1); m_yuv_l1 = m + random.uniform(0, ran_scale)
    image_yuv_l2 = alter_luminance(image_yuv, w1=0.8, w2=0.2); m_yuv_l2 = m + random.uniform(0, ran_scale)    
    
    m_f = -1.0*m 
    image_yuv_f = cv2.flip(image_yuv,1); m_yuv_f = m_f + random.uniform(-ran_scale, 0)
    image_yuv_f_r = cv2.flip(image_yuv_r,1); m_yuv_f_r = m_f + random.uniform(-ran_scale, 0)
    image_yuv_f_l1 = cv2.flip(image_yuv_l1,1); m_yuv_f_l1 = m_f + random.uniform(-ran_scale, 0)
    image_yuv_f_l2 = cv2.flip(image_yuv_l2,1); m_yuv_f_l2 = m_f + random.uniform(-ran_scale, 0)  
    
    image_aug.extend((image_yuv, image_yuv_r, image_yuv_l1, image_yuv_l2, 
                      image_yuv_f, image_yuv_f_r, image_yuv_f_l1, image_yuv_f_l2))
    m_aug.extend((m_yuv, m_yuv_r, m_yuv_l1, m_yuv_l2, 
                  m_yuv_f, m_yuv_f_r, m_yuv_f_l1, m_yuv_f_l2))   
    
    return image_aug, m_aug

def get_partial_augmented_data_nvidia(image_bgr, m):
    image_aug = []
    m_aug = []
    image_yuv = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2YUV); m_yuv = m   
    
    m_f = -1.0*m 
    image_yuv_f = cv2.flip(image_yuv,1); m_yuv_f = m_f
    
    image_aug.extend((image_yuv, image_yuv_f))
    m_aug.extend((m_yuv, m_yuv_f))
    
    return image_aug, m_aug

def extend_data(images, measurements, c_image_aug, c_m_aug, l_image_aug, l_m_aug, r_image_aug, r_m_aug):
    
    images.extend(c_image_aug)
    images.extend(l_image_aug)
    images.extend(r_image_aug)
    measurements.extend(c_m_aug)
    measurements.extend(l_m_aug)
    measurements.extend(r_m_aug)

    return images, measurements

def generator_3_aug_nvidia_special(image_files_path, samples, split_on, batch_size=32, correction = 0.25):
    num_samples = len(samples)
    while 1: # Loop forever so the generator never terminates
        samples = shuffle(samples)
        for offset in range(0, num_samples, batch_size):
            batch_samples = samples[offset:offset+batch_size]

            images = []
            measurements = []

            for batch_sample in batch_samples:
                center_camera_path = image_files_path + batch_sample[0].split(split_on)[-1]   # path for the image from central camera
                left_camera_path = image_files_path + batch_sample[1].split(split_on)[-1]   # path for the image from left camera
                right_camera_path = image_files_path + batch_sample[2].split(split_on)[-1]   # path for the image from right camera

                c_image_bgr = cv2.imread(center_camera_path) # openCV reads an image in BGR format
                c_image_bgr = cv2.resize(c_image_bgr, (200, 101), interpolation = cv2.INTER_CUBIC)
                l_image_bgr = cv2.imread(left_camera_path)
                l_image_bgr = cv2.resize(l_image_bgr, (200, 101), interpolation = cv2.INTER_CUBIC)
                r_image_bgr = cv2.imread(right_camera_path)
                r_image_bgr = cv2.resize(r_image_bgr, (200, 101), interpolation = cv2.INTER_CUBIC)

                c_m = float(batch_sample[3]) # steering angle value for the center camera
                l_m = c_m + correction
                r_m = c_m - correction

                if (abs(c_m) <= 0.7):

                    if (abs(c_m) >= 0.001):
                        c_image_aug, c_m_aug = get_augmented_data_nvidia(c_image_bgr, c_m)  
                        l_image_aug, l_m_aug = get_augmented_data_nvidia(l_image_bgr, l_m)
                        r_image_aug, r_m_aug = get_augmented_data_nvidia(r_image_bgr, r_m)
                        images, measurements = extend_data(images, measurements, 
                                                           c_image_aug, c_m_aug, l_image_aug, l_m_aug, r_image_aug, r_m_aug)

                        if (abs(c_m) >= 0.15):
							c_image_aug, c_m_aug = get_augmented_data_nvidia(c_image_bgr, c_m)  
							l_image_aug, l_m_aug = get_augmented_data_nvidia(l_image_bgr, l_m)
							r_image_aug, r_m_aug = get_augmented_data_nvidia(r_image_bgr, r_m)
                            images, measurements = extend_data(images, measurements,
                                                               c_image_aug, c_m_aug, l_image_aug, l_m_aug, r_image_aug, r_m_aug)
                        if (abs(c_m) >= 0.5):
							c_image_aug, c_m_aug = get_augmented_data_nvidia(c_image_bgr, c_m)  
							l_image_aug, l_m_aug = get_augmented_data_nvidia(l_image_bgr, l_m)
							r_image_aug, r_m_aug = get_augmented_data_nvidia(r_image_bgr, r_m)
                            images, measurements = extend_data(images, measurements,
                                                               c_image_aug, c_m_aug, l_image_aug, l_m_aug, r_image_aug, r_m_aug)

                    else:   
                        c_image_aug, c_m_aug = get_partial_augmented_data_nvidia(c_image_bgr, c_m)  
                        l_image_aug, l_m_aug = get_partial_augmented_data_nvidia(l_image_bgr, l_m)
                        r_image_aug, r_m_aug = get_partial_augmented_data_nvidia(r_image_bgr, r_m)
                        images, measurements = extend_data(images, measurements,
                                                           c_image_aug, c_m_aug, l_image_aug, l_m_aug, r_image_aug, r_m_aug)

                
            X_train = np.array(images)            
            y_train = np.array(measurements)
            
            yield sklearn.utils.shuffle(X_train, y_train)  

def run_nvidia(train_generator, validation_generator, input_shape, n_train_samples, n_validation_samples, 
               num_epochs, model_file, model_exist_flag = False):    

    if (model_exist_flag == True):
        model = load_model(model_file)     
        adam = Adam(lr=0.0005)  
    else:  
        model = Sequential()
        model.add(Lambda(lambda x:(x/255.0)-0.5, input_shape=input_shape))
        model.add(Cropping2D(cropping=((25, 10), (0, 0))))

        model.add(Conv2D(24,5,5, subsample=(2,2), activation="elu")) # subsample is same as strides
        model.add(Conv2D(36,5,5, subsample=(2,2), activation="elu"))
        model.add(Conv2D(48,5,5, subsample=(2,2), activation="elu"))
        model.add(Conv2D(64,3,3, activation="elu"))
        model.add(Conv2D(64,3,3, activation="elu"))
        model.add(Flatten())
        model.add(Dense(100))
        model.add(Dense(50))
        model.add(Dense(10))
        model.add(Dense(1))
        adam = Adam(lr=0.001)
        
    model.compile(loss='mse', optimizer=adam)
    history_object = model.fit_generator(train_generator, samples_per_epoch = n_train_samples,
                                         validation_data = validation_generator, 
                                         nb_val_samples=n_validation_samples, nb_epoch=num_epochs, verbose = 1)

    model.save(model_file)
    
    return history_object

def execute_pipeline(driving_log_path, image_files_path, split_on, model_exist_flag, 
                     batch_size, num_epochs, approach, arch, input_shape, correction): 
    
    train_samples, validation_samples = get_train_validation_samples(driving_log_path, image_files_path)

    model_file = '.\car_model' + arch + approach + '.h5'
    
    train_generator = generator_3_aug_nvidia_special(train_samples, split_on = split_on, 
                                             batch_size=batch_size, correction=correction)
    validation_generator = generator_3_aug_nvidia_special(validation_samples, split_on = split_on, 
                                                  batch_size=batch_size, correction=correction)    
    s = 19
    
    history_object = run_nvidia(train_generator, validation_generator, input_shape, 
                                s*len(train_samples), s*len(validation_samples), num_epochs, 
                                model_file, model_exist_flag = model_exist_flag)
    
    plot_history_object(history_object, approach, arch, model_exist_flag)
    
    return history_object
    
    
def plot_history_object(history_object, approach, arch, model_exist_flag):
    
    ### print the keys contained in the history object
    print(history_object.history.keys())

    ### plot the training and validation loss for each epoch
    plt.figure()
    plt.plot(history_object.history['loss'])
    plt.plot(history_object.history['val_loss'])
    plt.title('model mean squared error loss')
    plt.ylabel('mean squared error loss')
    plt.xlabel('epoch')
    plt.legend(['training set', 'validation set'], loc='upper right')
    if model_exist_flag:
        sFile = 'Performance' + approach + arch + '_tune.png'
    else:
        sFile = 'Performance' + approach + arch + '.png'
    plt.savefig(sFile, bbox_inches='tight', orientation='landscape', dpi=300)

    return 
