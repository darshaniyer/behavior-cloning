# Behavioral cloning

A self-driving car is an artificially intelligent (AI) robot on wheels. It is equipped with cameras and other sensors such as radar, lidar, GPS, to recognize its environment and navigate through it. As an initial step of building that AI brain of a self-driving car is to develop it in a simulated environment provided by simulator, a process called behavioral cloning. In behavioral cloning, the goal is to make a car drive like oneself. For example, Udacity provides a simulator with a car that is equipped with three front-facing cameras - left, center, and right that provide video streams and records the values of the steering angle, speed, throttle and brake. The images from video streams and corresponding steering angles are fed into an AI brain formed by a network of convolutional neural network (CNN) and fully connected neural network. As shown in the Figure 1 ([Nvidia paper](http://images.nvidia.com/content/tegra/automotive/images/2016/solutions/pdf/end-to-end-dl-using-px.pdf)) below, during the training phase, the network parameters of the AI brain are trained to predict the steering angles based on the input camera images through a process of forward and back-propogation. If the model is good, it can successfully predict what angle was being driven at a given moment. Once the model is trained to satisfaction, one can test the model by launching the simulator and entering autonomous mode. As shown in the Figure 2 below, during the inference phase, the trained network predicts the steering angles from the input camera images to navigate its way through the environment.

[image1]: ./figures/Training.jpg "Figure 1: Training phase"
![alt text][image1]
**Figure 1: Training phase**

[image2]: ./figures/Inference.jpg "Figure 2: Inference phase"
![alt text][image2]
**Figure 2: Inference phase**

During inference, if the car is driving off the track, this could be because of the inadequacy of the data collection process. It is important to feed the network example of good driving behavior so that the vehicle stays in the center and recovers when getting too close to the sides of the road.

## Overall steps

The goals/steps of this project are the following:

* Use the simulator to collect data of good driving behavior.
* Build, a convolution neural network in Keras that predicts steering angles from images.
* Train and validate the model with a training and validation set.
* Test that the model successfully drives around track one without leaving the road.
* Summarize the results with a written report.

## Dependencies

This project requires:

* [CarND Term1 Starter Kit](https://github.com/udacity/CarND-Term1-Starter-Kit)

The lab environment can be created with CarND Term1 Starter Kit. Click [here](https://github.com/udacity/CarND-Term1-Starter-Kit/blob/master/README.md) for the details.

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Behavioral-Cloning-P3).

Instructions for downloading the simulator can be found [here](https://github.com/udacity/self-driving-car-sim).

## Detailed Writeup

Detailed report can be found in [_Behavioral_cloning-writeup.md_](./Behavioral_cloning-writeup.md) 

## Solution video

![](./videos/BehavioralCloning_demo.mp4)

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. I received some inspiration regarding the importance of data augmentation by reading  the blogs of previous attempts, although no code or any idea was used from previous work. I used some plots from the Udacity training material and Nvidia paper.


